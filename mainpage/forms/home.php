<?php
session_start();

require_once "config.php";
$nav_select = "SELECT * FROM Stays";
$result = mysqli_query($conn, $nav_select);
// print_r($result);
$navs = mysqli_fetch_all($result);
// echo "<pre>";
// print_r($navs);
// echo "</pre>";

if (isset($_SESSION['id']) && isset($_SESSION['user_name'])) {


?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
    <link rel="stylesheet" href="home.css">
    <title>booking.com</title>
</head>

<body>

    <header>
        <h1>booking</h1>






        <a class="a1" href="index.php"><i class="fa-solid fa-bed"></i> stays</a>
        <a href="Flights.php"><i class="fa fa-plane"></i> Flights</a>
        <a href="CarRentals.php"><i class="fa fa-car"></i> Car Rentals</a>
        <a href="Attractions.php"><i class="fa-solid fa-ferris-wheel" style="color: #ffffff;"></i> Attractions</a>
        <a href="AirportTaxis.php"><i class="fa-solid fa-taxi"></i> Airport Taxis</a>
        <a class="logout" href="logout.php">Logout</a>
        <br><br><br>
        <h1>Find your next stay <?php echo $_SESSION['name']; ?></h1>
        <h3>Search low prices on hotels, homes and much more...</h3>
        <br><br><br>


    </header>



    <div class="search-bar">
        <div class="search-bar-inputs">
            <div class="input-group">
                <label for="destination">Destination</label>
                <input type="text" id="destination" placeholder="Where are you going?" class="destination">

            </div>
            <div class="input-group">
                <label for="check-in">Check-in</label>
                <input type="date" id="check-in">
            </div>
            <div class="input-group">
                <label for="check-out">Check-out</label>
                <input type="date" id="check-out">
            </div>
            <div class="input-group">
                <label for="guests">Guests</label>
                <select id="guests">
                    <option value="1">1 guest</option>
                    <option value="2">2 guests</option>
                    <option value="3">3 guests</option>
                </select>
            </div>
        </div>
        <button type="button">Search</button>
    </div>
    <br><br><br>

    <h1>Trending Destinations</h1>
    <h4>Travellers searching for Georgia also booked these</h4>

    <section>
        <ul>

            <?php
            foreach ($navs as $cities) {
            ?>
                <li><a class="cities" href="?topic=<?= $cities[0] ?>"><?= $cities[1] ?></a></li>
            <?php
            }
            ?>
        </ul>
    </section>
    <br>

    <footer>
        &copy; 2024 My Booking Site. All rights reserved.
    </footer>

</body>

</html>


<?php 
}else{
     header("Location: index.php");
     exit();
}
 ?>