<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
    <link rel="stylesheet" href="STYLE.css">
    <title>Attractions</title>
</head>

<body>
    <header>
        <h1>booking</h1>


        <a href="index.php"><i class="fa-solid fa-bed"></i> stays</a>
        <a href="Flights.php"><i class="fa fa-plane"></i> Flights</a>
        <a href="CarRentals.php"><i class="fa fa-car"></i> Car Rentals</a>
        <a class="a1" href="Attractions.php"><i class="fa-solid fa-ferris-wheel" style="color: #ffffff;"></i> Attractions</a>
        <a href="AirportTaxis.php"><i class="fa-solid fa-taxi"></i> Airport Taxis</a>
        <a class="log" href="forms/index.php">Log In</a>
        <a class="reg" href="forms/register.php">Register</a>


    </header>

    <footer>
        &copy; 2024 My Booking Site. All rights reserved.
    </footer>

</body>

</html>